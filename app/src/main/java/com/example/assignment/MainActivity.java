package com.example.assignment;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    Button btnLogin;
    EditText edtUserID;
    EditText edtPassword;
    UserDB db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnLogin = (Button) findViewById(R.id.btnLogin);
        edtUserID = (EditText) findViewById(R.id.edtUserID);
        edtPassword = (EditText) findViewById(R.id.edtPassword);

        db = new UserDB(MainActivity.this);

        btnLogin.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                boolean isExist = db.checkUserExist(edtUserID.getText().toString(), edtPassword.getText().toString());

                if(isExist){
                    Intent intent = new Intent(MainActivity.this, ViewSchedule.class);
                    intent.putExtra("userID", edtUserID.getText().toString());
                    startActivity(intent);
                }else{
                    edtPassword.setText(null);
                    Toast.makeText(MainActivity.this, "Login Failed.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
