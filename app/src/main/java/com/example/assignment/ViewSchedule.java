package com.example.assignment;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static android.graphics.Color.*;

public class ViewSchedule extends AppCompatActivity {

    private Button btnSchedule;
    private TextView thedate, selectdate;
    private static final String TAG = "Schedual";
    private CompactCalendarView mySchedual;

    private DatePickerDialog.OnDateSetListener mDatepicker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_schedule);
        thedate = (TextView) findViewById(R.id.thedate);
        mySchedual = (CompactCalendarView) findViewById(R.id.compactcalendar_view);

        Calendar calendar = Calendar.getInstance();
        final DateFormat df = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        String currentDate = df.format(calendar.getTime());
        thedate.setText(currentDate);

        final ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(false);
        actionbar.setTitle(null);
        mySchedual.setUseThreeLetterAbbreviation(true);

        final Event ev1 = new Event(RED, 1579298400000L, "CSE2MAD exam");
        mySchedual.addEvent(ev1);


        mySchedual.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                Context context = getApplicationContext();
                Log.d(TAG, "Day was clicked: " + dateClicked + " with events " + ev1);

                if(dateClicked.toString().compareTo("Sat Jan 18 00:00:00 GMT+11:00 2020") == 0){
                    Toast.makeText(context, "CSE2MAD exam", Toast.LENGTH_SHORT).show();

            }else{
                Toast.makeText(context, "No event planned", Toast.LENGTH_SHORT).show();
            }
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                actionbar.setTitle(df.format(firstDayOfNewMonth));
            }
        });
        selectdate = (TextView) findViewById(R.id.selectdate);
        selectdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog =new DatePickerDialog(
                        ViewSchedule.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDatepicker,
                        year,month,day);
                dialog.getWindow().setBackgroundDrawable((new ColorDrawable((TRANSPARENT))));
                dialog.show();
            }
        });

        mDatepicker = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;
                String date = month + "/" + dayOfMonth + "/" + year;
                selectdate.setText(date);
            }
        };


    }
    }
